package edu.ifpb.dac.main;

import edu.ifpb.dac.dominio.Dependente;
import edu.ifpb.dac.dominio.Empregado;
import edu.ifpb.dac.dominio.Endereco;
import edu.ifpb.dac.dominio.Faculdade;
import edu.ifpb.dac.dominio.Projeto;
import edu.ifpb.dac.infraestrutura.Dao;
import edu.ifpb.dac.infraestrutura.DaoJPA;

/**
 *
 * @author Ricardo Job
 */
public class CadastraEmpregado {

    public static void main(String[] args) {
        Dao dao = new DaoJPA("relacionamento");

        Empregado empregado = new Empregado();

        Endereco endereco = new Endereco("Sua rua", "Seu Bairro", empregado);
        Dependente dependente = new Dependente("Alguem");

        empregado.setNome("Ricardo  de Sousa Job");
        empregado.addDep(dependente);
        empregado.setEndereco(endereco);

        Projeto projeto = new Projeto();
        projeto.setDescricao("Empresa Junior");
        projeto.addEmpregado(empregado);

        empregado.addProj(projeto);

        Faculdade faculdade = new Faculdade("IFPB");
        empregado.setFaculdade(faculdade);

        dao.save(empregado);
    }

}
